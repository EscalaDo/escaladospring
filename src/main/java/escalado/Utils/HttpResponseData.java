package escalado.Utils;

import org.springframework.http.HttpStatus;

public class HttpResponseData {

	private HttpStatus status;
	private String message;
	
	public HttpResponseData(HttpStatus status, String message) {
		this.status = status;
		this.message = message;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}
	
}

package escalado.Utils;

import escalado.Models.User;

public class LoginResponse {

	private String token;
	private User user;
	private String message;
	public LoginResponse(String token, User user) {
		this.token = token;
		this.user = user;
	}
	public LoginResponse(String message) {
		this.message = message;
	}
	public String getMessage() {
		return message;
	}
	public String getToken() {
		return token;
	}
	public User getUser() {
		return user;
	}
	
	
	
}

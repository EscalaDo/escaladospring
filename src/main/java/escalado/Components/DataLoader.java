package escalado.Components;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import escalado.Models.ClimbingGym;
import escalado.Models.Comment;
import escalado.Models.DifficultyLevel;
import escalado.Models.Exercise;
import escalado.Models.GradingSystem;
import escalado.Models.GripType;
import escalado.Models.Image;
import escalado.Models.Role;
import escalado.Models.Route;
import escalado.Models.Skill;
import escalado.Models.Training;
import escalado.Models.User;
import escalado.Services.ClimbingGymService;
import escalado.Services.CommentService;
import escalado.Services.DifficultyLevelService;
import escalado.Services.ExerciseService;
import escalado.Services.GradingSystemService;
import escalado.Services.GripTypeService;
import escalado.Services.ImageService;
import escalado.Services.RoleService;
import escalado.Services.RouteService;
import escalado.Services.SkillService;
import escalado.Services.TrainingService;
import escalado.Services.UserService;
import escalado.Services.Auth.BcryptPasswordEncoderService;

@Component
public class DataLoader implements ApplicationRunner {
	//test
	private final static Logger LOGGER =  Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private final static String PATH = "/home/debian/EscaladoImages/";
	//private final static String PATH = "/Users/amina/imagesdebian";

	@Autowired
	SkillService skillServ;
	
	@Autowired
	GripTypeService gripServ;
	
	@Autowired
	GradingSystemService gsServ;
	
	@Autowired
	DifficultyLevelService dlServ;
	
	@Autowired
	UserService userServ;
	
	@Autowired
	RoleService rServ;
	
	@Autowired
	RouteService routeServ;
	
	@Autowired
	ImageService imgServ;
	
	@Autowired
	CommentService comServ;
	
	@Autowired
	ExerciseService exServ;
	
	@Autowired
	TrainingService trainServ;
	
	@Autowired
	ClimbingGymService cgServ;
	
	@Autowired
	BcryptPasswordEncoderService encoder;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		String result = "";
		//skills
		Skill skill1 = new Skill("Dynamic");
		Skill skill2 = new Skill("Strenght");
		Skill skill3 = new Skill("Static");
		Skill skill4 = new Skill("Balance");
		skillServ.insert(skill1);
		skillServ.insert(skill2);
		skillServ.insert(skill3);
		skillServ.insert(skill4);
		result = result + "Added skills\n";
		//grip types
		GripType grip1 = new GripType("Pocket");
		GripType grip2 = new GripType("Pinch");
		GripType grip3 = new GripType("Jug");
		GripType grip4 = new GripType("Crimp");
		GripType grip5 = new GripType("Edge");
		GripType grip6 = new GripType("Sloper");
		GripType grip7 = new GripType("Undercling");
		GripType grip8 = new GripType("Flake");
		GripType grip9 = new GripType("Horn");
		
		gripServ.insert(grip5);
		gripServ.insert(grip4);
		gripServ.insert(grip3);
		gripServ.insert(grip2);
		gripServ.insert(grip1);
		gripServ.insert(grip6);
		gripServ.insert(grip7);
		gripServ.insert(grip8);
		gripServ.insert(grip9);
		result = result + "Added Grip Types\n";
		
		//grading systems
		GradingSystem YDS = new GradingSystem("YDS");
		GradingSystem french = new GradingSystem("French");
		gsServ.insert(french);
		gsServ.insert(YDS);
		result = result + "Added Grading Systems\n";
		//difficulty levels
		
		DifficultyLevel dl1 = new DifficultyLevel("1", french);
		DifficultyLevel dl2 = new DifficultyLevel("2", french);
		DifficultyLevel dl3 = new DifficultyLevel("3", french);
		DifficultyLevel dl4 = new DifficultyLevel("4", french);
		DifficultyLevel dl5 = new DifficultyLevel("5", french);
		
		DifficultyLevel dl6a = new DifficultyLevel("6a", french);
		DifficultyLevel dl6ap = new DifficultyLevel("6a+", french);
		DifficultyLevel dl6b = new DifficultyLevel("6b", french);
		DifficultyLevel dl6bp = new DifficultyLevel("6b+", french);
		DifficultyLevel dl6c = new DifficultyLevel("6c", french);
		DifficultyLevel dl6cp = new DifficultyLevel("6c+", french);

		DifficultyLevel dl7a = new DifficultyLevel("7a", french);
		DifficultyLevel dl7ap = new DifficultyLevel("7a+", french);
		DifficultyLevel dl7b = new DifficultyLevel("7b", french);
		DifficultyLevel dl7bp = new DifficultyLevel("7b+", french);
		DifficultyLevel dl7c = new DifficultyLevel("7c", french);
		DifficultyLevel dl7cp = new DifficultyLevel("7c+", french);
		
		DifficultyLevel dl8a = new DifficultyLevel("8a", french);
		DifficultyLevel dl8ap = new DifficultyLevel("8a+", french);
		DifficultyLevel dl8b = new DifficultyLevel("8b", french);
		DifficultyLevel dl8bp = new DifficultyLevel("8b+", french);
		DifficultyLevel dl8c = new DifficultyLevel("8c", french);
		DifficultyLevel dl8cp = new DifficultyLevel("8c+", french);
		
		DifficultyLevel dl9a = new DifficultyLevel("9a", french);
		DifficultyLevel dl9ap = new DifficultyLevel("9a+", french);
		DifficultyLevel dl9b = new DifficultyLevel("9b", french);
		DifficultyLevel dl9bp = new DifficultyLevel("9b+", french);
		DifficultyLevel dl9c = new DifficultyLevel("9c", french);
		DifficultyLevel dl9cp = new DifficultyLevel("9c+", french);
		
		dlServ.insert(dl1);
		dlServ.insert(dl2);
		dlServ.insert(dl3);
		dlServ.insert(dl4);
		dlServ.insert(dl5);
		
		dlServ.insert(dl6a);
		dlServ.insert(dl6ap);
		dlServ.insert(dl6b);
		dlServ.insert(dl6bp);
		dlServ.insert(dl6c);
		dlServ.insert(dl6cp);
		
		dlServ.insert(dl7a);
		dlServ.insert(dl7ap);
		dlServ.insert(dl7b);
		dlServ.insert(dl7bp);
		dlServ.insert(dl7c);
		dlServ.insert(dl7cp);

		dlServ.insert(dl8a);
		dlServ.insert(dl8ap);
		dlServ.insert(dl8b);
		dlServ.insert(dl8bp);
		dlServ.insert(dl8c);
		dlServ.insert(dl8cp);
		
		dlServ.insert(dl9a);
		dlServ.insert(dl9ap);
		dlServ.insert(dl9b);
		dlServ.insert(dl9bp);
		dlServ.insert(dl9c);
		dlServ.insert(dl9cp);
		
		//roles
		Role climber = new Role("Climber");
		Role climbGymAdmin = new Role("Climbing Gym");
		
		rServ.insert(climber);
		rServ.insert(climbGymAdmin);
				
		//users
		User climberUser = new User("Raul", "Ramirez", "ramsanraul", "ramsanraul@gmail.com", climber,encoder.encode("super3"));
		User climberUser2 = new User("Amina", "Khyat", "akhyat", "amina.khyat@gmail.com", climber, encoder.encode("super3"));
		User climberUser3 = new User("Adrian", "Naise", "anaise", "adriannaise@gmail.com", climbGymAdmin, encoder.encode("super3"));
		User climberUser4 = new User("Eloi", "Vazquez", "evazquez", "evazquez@ies-sabadell.cat", climber, encoder.encode("super3"));
		userServ.insert(climberUser3);
		ClimbingGym cg1 = new ClimbingGym("La panxa del bou", "Sabadell");
//		cgServ.insertAsObj(cg1);

		ClimbingGym cg2 = new ClimbingGym("Sharma Gava", "Gava");
		ClimbingGym cg3 = new ClimbingGym("Flashh", "Sant Cugat del Valles");
		cgServ.insertAsObj(cg1);
		cgServ.insertAsObj(cg2);
		cgServ.insertAsObj(cg3);
		userServ.insert(climberUser);
		userServ.insert(climberUser2);
		userServ.insert(climberUser4);
		userServ.insert(climberUser3);



		result = result + "Added Users\n";
		
		//add vias
		loadVias();
		loadTrainings();
	}
	
	private void loadTrainings() {
		User u = userServ.findById(2);
		User u1 = userServ.findById(4);
		Training t1 = new Training("Strength", u);
		trainServ.insertTraining(t1);
		Exercise ex1 = new Exercise("Push ups", 3, 12, t1);
		Exercise ex2 = new Exercise("Pull ups", 3, 12, t1);
		Exercise ex3 = new Exercise("Muscle ups", 3, 12, t1);
		Exercise ex4 = new Exercise("Sit ups", 3, 12, t1);
		exServ.insertExercise(ex1);
		exServ.insertExercise(ex2);
		exServ.insertExercise(ex3);
		exServ.insertExercise(ex4);

		User u2 = userServ.findById(1);
		u2.getTrainings().add(t1);
		userServ.edit(u2);
		
		Training t2 = new Training("Conditioning", u);
		trainServ.insertTraining(t2);
		Exercise ex5 = new Exercise("L-sits", 3, 12, t2);
		Exercise ex6 = new Exercise("Biceps curls", 3, 12, t2);
		Exercise ex7 = new Exercise("Muscle ups", 3, 12, t2);
		exServ.insertExercise(ex5);
		exServ.insertExercise(ex6);
		exServ.insertExercise(ex7);
		
		Training t3 = new Training("Elasticity", u1);
		trainServ.insertTraining(t3);
		Exercise ex8 = new Exercise("Splits", 3, 10, t3);
		Exercise ex9 = new Exercise("Biceps curls", 3, 12, t3);
		Exercise ex10 = new Exercise("Muscle ups", 3, 12, t3);
		exServ.insertExercise(ex8);
		exServ.insertExercise(ex9);
		exServ.insertExercise(ex10);
	}

	private void loadVias() {
		User u = userServ.findById(1);
		User u2 = userServ.findById(2);
		Skill skill = skillServ.findById(1);
		GripType gt = gripServ.findById(1);
		DifficultyLevel dl = dlServ.findById(1);
		DifficultyLevel dl2 = dlServ.findById(10);

		
		File img1 = new File("/home/debian/escalado/images/image1.jpg");
		File img2 = new File("/home/debian/escalado/images/image2.jpg");
		File img3 = new File("/home/debian/escalado/images/image3.jpg");
		File img4 = new File("/home/debian/escalado/images/image4.jpg");
		File img5 = new File("/home/debian/escalado/images/image5.jpg");
		File img6 = new File("/home/debian/escalado/images/image6.jpg");

		//createDirIfNotExist(u.getId()+"/");
		//createDirIfNotExist(u2.getId()+"/");
		
		/**
		//local
		File img1 = new File("/Users/amina/images/image1.jpg");
		File img2 = new File("/Users/amina/images/image2.jpg");
		File img3 = new File("/Users/amina/images/image3.jpg");
		File img4 = new File("/Users/amina/images/image4.jpg");
		**/
		//createDirIfNotExist(u.getId()+"/");
		//createDirIfNotExist(u2.getId()+"/");
		Route route1 = new Route(5, "gran ruta", u, skill, gt, dl, null);
		Route route2 = new Route(8, "Took a few attempts but made it!", u2, skill, gt, dl, null);
		Route route3 = new Route(4, "What an amazing route!", u2, skill, gt, dl2, null);
		Route route4 = new Route(1, "Really difficult route!", u2, skill, gt, dl2, null);

		routeServ.addRoute(route1);
		routeServ.addRoute(route2);
		routeServ.addRoute(route3);
		routeServ.addRoute(route4);
		//String userPath = u.getId()+"/";
		//String userPath2 = u2.getId()+"/";
		try {
		    FileUtils.copyFileToDirectory(img1, new File(PATH));
		    FileUtils.copyFileToDirectory(img2, new File(PATH));
		    FileUtils.copyFileToDirectory(img3, new File(PATH));
		    FileUtils.copyFileToDirectory(img4, new File(PATH));
		    FileUtils.copyFileToDirectory(img5, new File(PATH));
		    FileUtils.copyFileToDirectory(img6, new File(PATH));
		    Image i1 = new Image("image1.jpg");
		    Image i2 = new Image("image2.jpg");
		    Image i3 = new Image("image3.jpg");
		    Image i4 = new Image("image4.jpg");
		    Image i5 = new Image("image5.jpg");
		    Image i6 = new Image("image6.jpg");
		    i1.setRoute(route1);
		    i2.setRoute(route1);
		    i3.setRoute(route2);
		    i4.setRoute(route2);
		    i5.setRoute(route3);
		    i6.setRoute(route4);
		    
		    imgServ.insert(i1);
		    imgServ.insert(i2);
		    imgServ.insert(i3);
		    imgServ.insert(i4);
		    imgServ.insert(i5);
		    imgServ.insert(i6);

		} catch ( Exception e) {
		    //TODO exception IOException
			LOGGER.info("Happens if executed on windows!");
		}
		
		Comment comment1 = new Comment("thats amazing congratz!", u2, route1);
		Comment comment2 = new Comment("Pretty good route nice job", u, route2);
		
		Comment comment3 = new Comment("I didnt manage to finish that one nice!", u, route3);
		Comment comment4 = new Comment("Though route!", u2, route4);
		
		comServ.insertar(comment1);
		comServ.insertar(comment2);
		comServ.insertar(comment3);
		comServ.insertar(comment4);
	}
	
	private void createDirIfNotExist(String userId) {
	    //create directory to save the files
		File file = new File(PATH+userId);
			if(!file.exists()) {
				if (file.mkdir() == true) { 
					LOGGER.log(Level.INFO, "Directory has been created successfully"); 
				} 
				else { 
					LOGGER.log(Level.WARNING, "Directory cannot be created");
				}
			}
	  }

}

package escalado.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import escalado.Models.DifficultyLevel;

public interface DifficultyLevelRepository extends JpaRepository<DifficultyLevel, Integer>{

}

package escalado.Repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import escalado.Models.Role;

public interface RoleRepository extends JpaRepository<Role, Integer>{

	Optional<Role> findByName(String name);
	
}

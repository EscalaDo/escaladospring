package escalado.Repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import escalado.Models.Like;

public interface LikeRepository extends JpaRepository<Like, Integer> {

	@Query(value="SELECT count(*) FROM likes WHERE routeid=:id", nativeQuery = true)
	int getLikesOfPost(@Param("id") int id);
	
	@Query(value="SELECT count(*) FROM likes WHERE routeid=:id and userid=:iduser", nativeQuery = true)
	int doesLikeExist(@Param("id") int id, @Param("iduser") int iduser);

	@Transactional
	@Modifying
	@Query(value="INSERT INTO likes (routeid, userid, created_at) VALUES (?1, ?2, now())", nativeQuery = true)
	void likeRoute(int routeid, int userid);
	
	@Transactional
	@Modifying
	@Query(value="DELETE FROM likes WHERE routeid=?1 and userid=?2", nativeQuery = true)
	void deleteLikeRoute(int routeid, int userid);
}

package escalado.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import escalado.Models.Goal;

public interface GoalRepository extends JpaRepository<Goal, Integer> {

}

package escalado.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import escalado.Models.Skill;

public interface SkillRepository extends JpaRepository<Skill, Integer>{

}

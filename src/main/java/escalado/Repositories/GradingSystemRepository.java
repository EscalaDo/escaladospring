package escalado.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import escalado.Models.GradingSystem;

public interface GradingSystemRepository extends JpaRepository<GradingSystem, Integer>{

}

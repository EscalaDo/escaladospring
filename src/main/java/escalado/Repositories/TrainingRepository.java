package escalado.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import escalado.Models.Training;

public interface TrainingRepository extends JpaRepository<Training, Integer> {
	
	@Query(value="SELECT * FROM trainings ORDER BY training_id DESC", nativeQuery = true)
	List<Training> allTrainingsDescOrder();
	
	@Query(value="SELECT training_id, name, created_at, userId FROM trainings as t INNER JOIN user_trainings as ut ON training_id=ut.training WHERE ut.user=:id ORDER BY training_id DESC", nativeQuery = true)
	List<Training> getTrainingsUserLiked(@Param("id") int id);
	
	@Query(value="SELECT * FROM trainings WHERE userid=:id ORDER BY training_id DESC", nativeQuery = true)
	List<Training> getTrainingsUserCreated(@Param("id") int id);
	
	@Query(value="SELECT count(*) FROM user_trainings WHERE user=:user and training=:training", nativeQuery = true)
	int isTrainingUserSaved(@Param("training") int training, @Param("user") int user);
}

package escalado.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import escalado.Models.GripType;

public interface GripTypeRepository extends JpaRepository<GripType, Integer>{

}

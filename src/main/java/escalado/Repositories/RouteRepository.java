package escalado.Repositories;

import java.util.List;

import javax.transaction.Transactional;

//import java.util.List;

//import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import escalado.Models.Route;

public interface RouteRepository extends JpaRepository<Route, Integer>  {

	//List<Route> findAll(Direction desc);

	@Query(value="SELECT * FROM routes WHERE publisher_id=:id ORDER BY route_id DESC", nativeQuery = true)
	List<Route> getRoutesUserCreated(@Param("id") int id);
	
	@Transactional
	@Modifying
	@Query(value="DELETE FROM routes WHERE route_id=?1", nativeQuery = true)
	void deleteRouteById(int id);
}

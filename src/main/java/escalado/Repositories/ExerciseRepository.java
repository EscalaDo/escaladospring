package escalado.Repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import escalado.Models.Exercise;

public interface ExerciseRepository extends JpaRepository<Exercise, Integer> {
	
	@Transactional
	@Modifying
	@Query(value="DELETE FROM exercises WHERE exerciseid=?1 and trainingid=?2", nativeQuery = true)
	void deleteExerciseFromTraining(int exerciseid, int trainingid);

}

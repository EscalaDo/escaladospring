package escalado.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import escalado.Models.Comment;

public interface CommentRepository extends JpaRepository<Comment, Integer>{

	
	@Query(value="SELECT * FROM comments WHERE routeid=:id", nativeQuery = true)
	List<Comment> getCommentsFromRoute(@Param("id") int id);
}

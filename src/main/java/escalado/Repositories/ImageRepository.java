package escalado.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import escalado.Models.Image;

public interface ImageRepository extends JpaRepository<Image, Integer> {

}

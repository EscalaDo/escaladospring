package escalado.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import escalado.Models.ClimbingGym;


public interface ClimbingGymRepository extends JpaRepository<ClimbingGym, Integer>{

	Boolean existsByName(String name);
}

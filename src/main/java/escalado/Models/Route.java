package escalado.Models;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.transaction.Transactional;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Routes")
@Transactional
public class Route {

	@Id
	@Column(name = "RouteId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "Attempts", nullable = true)
	private int attempts;

	@Column(name = "Description", nullable = true)
	private String description;

	@ManyToOne
	@JoinColumn(name = "DifficultyLevelId")
	@JsonManagedReference
	private DifficultyLevel routeDifficultyLevel;

	@ManyToOne
	@JoinColumn(name = "GripTypeId")
	@JsonManagedReference
	private GripType routeGripType;

	@ManyToOne
	@JoinColumn(name = "PublisherId")
	@JsonManagedReference
	private User publisher;

	@ManyToOne
	@JoinColumn(name = "SkillId")
	@JsonManagedReference
	private Skill routeSkill;

	@ManyToOne(optional = true)
	@JoinColumn(name = "ClimbingGymId", nullable = true)
	@JsonManagedReference
	private ClimbingGym climbingGym;

	@Column(name = "CreatedAt")
	private Date timestamp;

	@OneToMany(mappedBy = "route", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	// @JsonBackReference
	private Set<Image> images;

	@OneToMany(mappedBy = "route", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	// @JsonBackReference
	private Set<Comment> comments;

	@OneToMany(mappedBy = "route", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	// @JsonBackReference
	private Set<Like> likes;

	public Route(int attempts, String description, User user, Skill skill, GripType gripType,
			DifficultyLevel difficultyLevel, ClimbingGym cg) {
		this.attempts = attempts;
		this.description = description;
		this.publisher = user;
		this.climbingGym = cg;

		this.routeSkill = skill;
		this.routeGripType = gripType;
		this.routeDifficultyLevel = difficultyLevel;
		this.likes = new HashSet<>();
		this.comments = new HashSet<>();
		this.images = new HashSet<>();
		this.timestamp = new Date();
	}

	public Route() {
		this.likes = new HashSet<>();
		this.comments = new HashSet<>();
		this.images = new HashSet<>();
		this.timestamp = new Date();
	}

	public int getAttempts() {
		return attempts;
	}

	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}

	public DifficultyLevel getRouteDifficultyLevel() {
		return routeDifficultyLevel;
	}

	public void setRouteDifficultyLevel(DifficultyLevel routeDifficultyLevel) {
		this.routeDifficultyLevel = routeDifficultyLevel;
	}

	public GripType getRouteGripType() {
		return routeGripType;
	}

	public void setRouteGripType(GripType routeGripType) {
		this.routeGripType = routeGripType;
	}

	public User getPublisher() {
		return publisher;
	}

	public void setPublisher(User publisher) {
		this.publisher = publisher;
	}

	public Skill getRouteSkill() {
		return routeSkill;
	}

	public void setRouteSkill(Skill routeSkill) {
		this.routeSkill = routeSkill;
	}

	public Set<Image> getImages() {
		return images;
	}

	public void setImages(Set<Image> images) {
		this.images = images;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public ClimbingGym getClimbingGym() {
		return climbingGym;
	}

	public void setClimbingGym(ClimbingGym climbingGym) {
		this.climbingGym = climbingGym;
	}

	public Set<Like> getLikes() {
		return likes;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Route other = (Route) obj;
		return id == other.id;
	}

}

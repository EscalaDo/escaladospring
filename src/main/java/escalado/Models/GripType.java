package escalado.Models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.transaction.Transactional;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "GripTypes")
@Transactional
public class GripType {

	@Id
	@Column(name = "GripTypeId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "Name", nullable = false, length = 25)
	private String name;
	@OneToMany(mappedBy = "routeGripType", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<Route> routes;

	public GripType(String name) {
		this.name = name;
		this.routes = new HashSet<Route>();
	}
	
	public GripType() {
		this.routes = new HashSet<Route>();
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Set<Route> getRoutes() {
		return routes;
	}

}

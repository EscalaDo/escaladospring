package escalado.Models;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.transaction.Transactional;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Likes")
@Transactional
public class Like {

	@Id
	@Column(name = "LikeID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "UserID")
	@JsonManagedReference
	private User user;

	@ManyToOne
	@JoinColumn(name = "RouteID")
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonBackReference
	private Route route;

	@Column(name = "CreatedAt")
	private Date timestamp;

	public Like(User user, Route route) {
		this.user = user;
		this.route = route;
		this.timestamp = new Date();
	}

	public Like() {
		this.timestamp = new Date();

	}

	public int getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public Route getRoute() {
		return route;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, route, user);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Like other = (Like) obj;
		return id == other.id && Objects.equals(route, other.route) && Objects.equals(user, other.user);
	}

}

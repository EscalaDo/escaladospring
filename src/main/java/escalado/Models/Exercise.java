package escalado.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.transaction.Transactional;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "Exercises")
@Transactional
public class Exercise {

	@Id
	@Column(name = "ExerciseID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "Name", nullable = false, length = 25)
	private String name;
	
	@Column(name = "Reps", nullable = true, length = 25)
	private int reps;
	
	@Column(name = "Sets", nullable = true, length = 25)
	private int sets;

	@ManyToOne
	@JoinColumn(name = "TrainingID")
	@JsonBackReference
	private Training training;

	public Exercise(String name, Training training) {
		this.name = name;
		this.training = training;
	}
	
	public Exercise(String name, int sets, int reps, Training training) {
		this.name = name;
		this.reps = reps;
		this.sets = sets;
		this.training = training;
	}

	public Exercise() {
		
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Training getTraining() {
		return training;
	}
	
	public int getReps() {
		return reps;
	}

	public void setReps(int reps) {
		this.reps = reps;
	}

	public int getSets() {
		return sets;
	}

	public void setSets(int sets) {
		this.sets = sets;
	}


}

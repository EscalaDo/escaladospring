package escalado.Models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.transaction.Transactional;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Goals")
@Transactional
public class Goal {

	@Id
	@Column(name = "GoalId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "Description", nullable = false, length = 200)
	private String description;
	@Column(name = "Achieved")
	private boolean achieved;
	@Column(name = "CreatedAt")
	private Date timestamp;

	@ManyToOne
	@JoinColumn(name = "UserID")
	@JsonManagedReference
	private User goalPublisher;

	public Goal(String description, User goalPublisher, boolean achieved) {
		this.description = description;
		this.timestamp = new Date();
		this.goalPublisher = goalPublisher;
		this.achieved = achieved;
	}

	public Goal() {
		this.timestamp = new Date();
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public User getGoalPublisher() {
		return goalPublisher;
	}

	public void setGoalPublisher(User goalPublisher) {
		this.goalPublisher = goalPublisher;
	}

	public boolean isAchieved() {
		return achieved;
	}

	public void setAchieved(boolean achieved) {
		this.achieved = achieved;
	}

}

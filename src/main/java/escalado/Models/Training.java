package escalado.Models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.transaction.Transactional;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Trainings")
@Transactional
public class Training {

	@Id
	@Column(name = "TrainingId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "Name", nullable = false, length = 40)
	private String name;

	@Column(name = "CreatedAt")
	private Date timestamp;

	@ManyToOne
	@JoinColumn(name = "UserID")
	@JsonManagedReference
	private User user;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "training", fetch = FetchType.EAGER)
	//@JsonBackReference
	private Set<Exercise> exercises;

	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "trainings", cascade = CascadeType.ALL)
	// @JoinTable(name = "RegioVeins", joinColumns = @JoinColumn(name = "veiDe"),
	// inverseJoinColumns = @JoinColumn(name = "vei"))
	@JsonManagedReference
	private Set<User> userTraining;

	public Training(String name, User user) {
		this.name = name;
		this.user = user;
		this.timestamp = new Date();
		this.exercises = new HashSet<>();
		this.userTraining = new HashSet<>();
	}

	public Training() {
		this.exercises = new HashSet<>();
		this.userTraining = new HashSet<>();
		this.timestamp = new Date();
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		return user;
	}

	public Set<Exercise> getExercises() {
		return exercises;
	}

	public Set<User> getUserTraining() {
		return userTraining;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setExercises(Set<Exercise> exercises) {
		this.exercises = exercises;
	}

	public void setUserTraining(Set<User> userTraining) {
		this.userTraining = userTraining;
	}

}

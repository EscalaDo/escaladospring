package escalado.Models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.transaction.Transactional;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "ClimbingGyms")
@Transactional
public class ClimbingGym {

	@Id
	@Column(name = "ClimbingGymId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "Name", nullable = false, length = 25)
	private String name;
	@Column(name = "Location", nullable = false, length = 25)
	private String location;

	@OneToMany(mappedBy = "climbingGym", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<Route> routes;


	public ClimbingGym(String name, String location) {
		this.name = name;
		this.location = location;
	}

	public ClimbingGym() {
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getLocation() {
		return location;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}

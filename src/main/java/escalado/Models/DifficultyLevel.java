package escalado.Models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.transaction.Transactional;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "DifficultyLevels")
@Transactional
public class DifficultyLevel {

	@Id
	@Column(name = "DifficultyLevelsId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "Name", nullable = false, length = 25)
	private String name;
	@ManyToOne
	@JoinColumn(name = "GradingSystemId")
	@JsonManagedReference
	private GradingSystem difficultyLevelGradingSystem;

	@OneToMany(mappedBy = "routeDifficultyLevel", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<Route> routes;

	public DifficultyLevel(String name, GradingSystem gs) {
		this.name = name;
		this.difficultyLevelGradingSystem = gs;
		this.routes = new HashSet<>();
	}

	public DifficultyLevel() {
		this.routes = new HashSet<>();
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public GradingSystem getDifficultyLevelGradingSystem() {
		return difficultyLevelGradingSystem;
	}

	public Set<Route> getRoutes() {
		return routes;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDifficultyLevelGradingSystem(GradingSystem difficultyLevelGradingSystem) {
		this.difficultyLevelGradingSystem = difficultyLevelGradingSystem;
	}

}

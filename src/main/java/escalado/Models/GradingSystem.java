package escalado.Models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.transaction.Transactional;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "GradingSystems")
@Transactional
public class GradingSystem {
	@Id
	@Column(name = "GradingSystemId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "Name", nullable = false, length = 25)
	private String name;

	@OneToMany(mappedBy = "difficultyLevelGradingSystem", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<DifficultyLevel> difficulties;

	public GradingSystem(String name) {
		this.name = name;
		this.difficulties = new HashSet<DifficultyLevel>();
	}
	
	public GradingSystem() {
		this.difficulties = new HashSet<DifficultyLevel>();
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Set<DifficultyLevel> getDifficulties() {
		return difficulties;
	}

}

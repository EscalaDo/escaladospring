package escalado.Models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.transaction.Transactional;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Comments")
@Transactional
public class Comment {

	@Id
	@Column(name = "CommentID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "Description", nullable = false, length = 128)
	private String description;

	@ManyToOne
	@JoinColumn(name = "UserID")
	@JsonManagedReference
	private User user;

	@ManyToOne
	@JoinColumn(name = "RouteID")
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonBackReference
	private Route route;

	@Column(name = "CreatedAt")
	private Date timestamp;

	public Comment(String description, User user, Route route) {
		this.description = description;
		this.user = user;
		this.route = route;
		this.timestamp = new Date();
	}

	public Comment() {
		this.timestamp = new Date();

	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public Date getTimestamp() {
		return timestamp;
	}

}

package escalado.Models;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.transaction.Transactional;
//import javax.validation.constraints.Email;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Users")
@Transactional
public class User {
	@Id
	@Column(name = "UserId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "Name", nullable = false, length = 25)
	private String name;
	@Column(name = "Lastname", nullable = false, length = 50)
	private String lastname;
	@Column(name = "Username", nullable = false, length = 50, unique = true)
	private String username;
	@Column(name = "Password", nullable = false, length = 80)
	private String password;
	@Column(name = "CreatedAt")
	private Date timestamp;

	// @Email
	@Column(name = "Email", nullable = false, length = 36)
	private String email;

	// un user solo tiene un rol, pero un rol lo pueden tener muchos usuarios
	@ManyToOne
	@JoinColumn(name = "RoleId")
	@JsonManagedReference
	private Role userRole;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "UserTrainings", joinColumns = @JoinColumn(name = "user"), inverseJoinColumns = @JoinColumn(name = "training"))
	@JsonBackReference
	private Set<Training> trainings;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "user", fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<Training> createdTrainings;

	@OneToMany(mappedBy = "publisher", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<Route> routes;
	
	@OneToMany(mappedBy = "goalPublisher", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<Goal> goals;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user", fetch = FetchType.EAGER)
	@JsonBackReference
	private Set<Like> likes;

	public User(String name, String lastname, String username, String email, Role role, String password) {
		this.name = name;
		this.lastname = lastname;
		this.username = username;
		this.email = email;
		this.userRole = role;
		this.password = password;

		this.likes = new HashSet<>();
		this.createdTrainings = new HashSet<>();
		this.routes = new HashSet<>();
		this.trainings = new HashSet<>();
		this.goals = new HashSet<>();
		this.timestamp = new Date();
	}

	public User() {
		this.likes = new HashSet<>();
		this.createdTrainings = new HashSet<>();
		this.routes = new HashSet<>();
		this.trainings = new HashSet<>();
		this.goals = new HashSet<>();
		this.timestamp = new Date();
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getLastname() {
		return lastname;
	}

	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Role getUserRole() {
		return userRole;
	}

	public Set<Training> getTrainings() {
		return trainings;
	}

	public Set<Training> getCreatedTrainings() {
		return createdTrainings;
	}

	public Set<Route> getRoutes() {
		return routes;
	}

	public Set<Like> getLikes() {
		return likes;
	}

	public void setTrainings(Set<Training> trainings) {
		this.trainings = trainings;
	}

	public void setCreatedTrainings(Set<Training> createdTrainings) {
		this.createdTrainings = createdTrainings;
	}

	public void setRoutes(Set<Route> routes) {
		this.routes = routes;
	}

	public void setLikes(Set<Like> likes) {
		this.likes = likes;
	}
	
	@JsonIgnore
	public String getPassword() {
		return password;
	}

	public Date getTimestamp() {
		return timestamp;
	}
	
	public Set<Goal> getGoals() {
		return goals;
	}

	public void setGoals(Set<Goal> goals) {
		this.goals = goals;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return id == other.id;
	}
}

package escalado.Models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.transaction.Transactional;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "Images")
@Transactional
public class Image {

	@Id
	@Column(name = "ImageId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "Name", nullable = false, length = 128)
	private String name;

	@Column(name = "CreatedAt")
	private Date timestamp;

	@ManyToOne
	@JoinColumn(name = "RouteId")
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonBackReference
	private Route route;

	public Image(String name) {
		this.name = name;
		this.timestamp = new Date();
	}

	public Image() {
		this.timestamp = new Date();

	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public Date getTimestamp() {
		return timestamp;
	}

}

package escalado.Controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import escalado.Services.ImageService;

@Controller
public class ImageController {

	@Autowired
	ImageService imServ;

	@ResponseBody
	@RequestMapping(value = "/image/{id}", method = RequestMethod.GET, consumes = MediaType.ALL_VALUE, produces = MediaType.IMAGE_PNG_VALUE)
	public ResponseEntity<byte[]> getImage(@PathVariable("id") int id) {

		byte[] image = getPNGImageById(id); // this just gets the data from a database
		return ResponseEntity.ok(image);
	}

	private byte[] getPNGImageById(int id) {

		try {
			File file = ResourceUtils.getFile("/home/debian/EscaladoImages/" + imServ.findById(id).getName());
			return Files.readAllBytes(file.toPath());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}

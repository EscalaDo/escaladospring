package escalado.Controllers.Requests;

public class RegisterHttpRequest {

	private String email;
	private String username;
	private String name;
	private String lastname;
	private int roleId;
	private String password;
	private String location;

	public RegisterHttpRequest(String email, String username, String name, String lastname, int roleId, String password,
			String location) {
		super();
		this.location = location;
		this.email = email;
		this.username = username;
		this.name = name;
		this.lastname = lastname;
		this.roleId = roleId;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public String getUsername() {
		return username;
	}

	public String getName() {
		return name;
	}

	public String getLastname() {
		return lastname;
	}

	public int getRoleId() {
		return roleId;
	}

	public String getPassword() {
		return password;
	}

	public String getLocation() {
		return location;
	}

}

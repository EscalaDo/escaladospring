package escalado.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import escalado.Models.Exercise;
import escalado.Models.Training;
import escalado.Services.ExerciseService;
import escalado.Services.TrainingService;
import escalado.Services.Auth.Requester.LoggedUser;
import escalado.Services.Auth.Requester.Requester;

@Controller
public class ExerciseController {

	@Autowired
	ExerciseService exerciseService;

	@Autowired
	TrainingService trainingService;

	@GetMapping("/getAllExercises")
	public @ResponseBody List<Exercise> getAllExercises() {
		return exerciseService.findAll();
	}

	@PostMapping("/createExercise")
	public ResponseEntity<String> createExercise(@RequestParam("name") String name, @RequestParam("sets") Integer sets,
			@RequestParam("reps") Integer reps, @RequestParam("trainingId") Integer trainingId, Requester requester) {
		if (requester instanceof LoggedUser) {
			Training t = trainingService.findById(trainingId);
			exerciseService.insertar(name, sets, reps, t);
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

	}

	@DeleteMapping("/deleteExercise/{id}")
	public ResponseEntity<Boolean> deleteExercise(@PathVariable int id, Requester requester) {
		if (requester instanceof LoggedUser) {
			Boolean isDeleted = exerciseService.deleteExerciseFromTraining(id);
			return ResponseEntity.ok().body(isDeleted);
		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

	}

}

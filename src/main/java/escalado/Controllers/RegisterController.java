package escalado.Controllers;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import escalado.Controllers.Requests.RegisterHttpRequest;
import escalado.Exceptions.EntityDoesNotExistException;
import escalado.Exceptions.InvalidPasswordException;
import escalado.Exceptions.UserDoesNotExistException;
import escalado.Exceptions.UsernameAlreadyExistsException;
import escalado.Models.User;
import escalado.Services.UserService;
import escalado.Utils.LoginResponse;

@Controller
public class RegisterController {

	@Autowired
	UserService userService;

//	@PostMapping("auth/register")
//	public ResponseEntity<String> register(@RequestBody RegisterHttpRequest request) {
//
//		try {
//			userService.register(request.getUsername(), request.getName(), request.getLastname(), request.getEmail(),
//					request.getPassword(), request.getRoleId());
//			return ResponseEntity.ok().build();
//		} catch (UsernameAlreadyExistsException e) {
//			return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
//		} catch (EntityDoesNotExistException e1) {
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e1.getMessage());
//		}
//
//	}
	
	@PostMapping("auth/register")
	public ResponseEntity<LoginResponse> register(@RequestBody RegisterHttpRequest request) {
		try {
			userService.register(request.getUsername(), request.getName(), request.getLastname(), request.getEmail(),
					request.getPassword(), request.getRoleId(), request.getLocation());
			String token = userService.login(request.getUsername(), request.getPassword());
			Optional<User> u = userService.getUserByUsername(request.getUsername());
			return ResponseEntity.ok().body(new LoginResponse(token, u.get()));
		} catch (UsernameAlreadyExistsException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(new LoginResponse(e.getMessage()));
		} catch (EntityDoesNotExistException e1) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new LoginResponse(e1.getMessage()));
		} catch (UserDoesNotExistException e2) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new LoginResponse(e2.getMessage()));
		} catch (InvalidPasswordException e3) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new LoginResponse(e3.getMessage()));
		} catch(NoSuchElementException e4) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new LoginResponse(e4.getMessage()));
		}
	}

}

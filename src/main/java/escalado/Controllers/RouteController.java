package escalado.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import escalado.Models.ClimbingGym;
import escalado.Models.DifficultyLevel;
import escalado.Models.GripType;
import escalado.Models.Route;
import escalado.Models.Skill;
import escalado.Models.User;
import escalado.Services.ClimbingGymService;
import escalado.Services.CommentService;
import escalado.Services.DifficultyLevelService;
import escalado.Services.GripTypeService;
import escalado.Services.ImageService;
import escalado.Services.LikeService;
import escalado.Services.RouteService;
import escalado.Services.SkillService;
import escalado.Services.UserService;
import escalado.Services.Auth.Requester.LoggedUser;
import escalado.Services.Auth.Requester.Requester;
/*import escalado.Services.Auth.Requester.LoggedUser;
import escalado.Services.Auth.Requester.Requester;*/
import escalado.Utils.HttpResponseData;

@Controller
public class RouteController {

	@Autowired
	UserService uServ;

	@Autowired
	ImageService iServ;

	@Autowired
	SkillService skServ;

	@Autowired
	GripTypeService gtServ;

	@Autowired
	DifficultyLevelService dlServ;

	@Autowired
	RouteService rService;

	@Autowired
	LikeService likeService;

	@Autowired
	CommentService commentService;

	@Autowired
	ClimbingGymService climbingGymService;

	@PostMapping("/uploadRoute")
	public ResponseEntity<String> createUserRoute(@RequestParam("files") MultipartFile[] files,
			@RequestParam("userId") Integer userId, @RequestParam("skillId") Integer skillId,
			@RequestParam("gripTypeId") Integer gripTypeId,
			@RequestParam("difficultyLevelId") Integer difficultyLevelId,
			@RequestParam("description") String description, @RequestParam("attempts") Integer attempts,
			@RequestParam("climbingGymId") Integer climbingGymId, Requester requester) {

		if (requester instanceof LoggedUser) {

			User u = uServ.findById(userId);

			String username = ((LoggedUser) requester).getUsername();
			if (u.getUsername().equals(username)) {
				Skill skill = skServ.findById(skillId);
				GripType gt = gtServ.findById(gripTypeId);
				DifficultyLevel dl = dlServ.findById(difficultyLevelId);
				ClimbingGym cg = climbingGymService.findById(climbingGymId);

				HttpResponseData response = rService.userAddRoute(files, u, skill, gt, dl, description, attempts, cg);
				System.out.println(skillId + " " + gripTypeId + " " + difficultyLevelId + " " + climbingGymId);
				return ResponseEntity.status(response.getStatus()).body(response.getMessage());

			} else {
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
			}

		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

	}

	@DeleteMapping("/deleteRoute/{id}")
	public ResponseEntity<Boolean> deleteRouteById(@PathVariable int id, Requester requester) {
		if (requester instanceof LoggedUser) {
			Route r = rService.findByRouteId(id);

			String username = ((LoggedUser) requester).getUsername();
			if (r.getPublisher().getUsername().equals(username)) {
				boolean deleted = rService.deleteRouteById(id);
				return ResponseEntity.ok().body(deleted);
			} else {
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
			}
		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

	}

	@PostMapping("/addOneImage")
	public ResponseEntity<String> addOneImage(@RequestParam("file") MultipartFile file) {

		HttpResponseData response = rService.addOneImage(file);

		return ResponseEntity.status(response.getStatus()).body(response.getMessage());
	}

	@GetMapping("/getAllRoutes")
	public @ResponseBody List<Route> getAllRoutes() {
		// List<Route> routes = rService.getAllRoutes();
		return rService.getAllRoutes();
	}

	// he cambiado para que devuelva true o false si cambia el like para no hacer
	// tantas peticiones a la api
	@PostMapping("/likeRoute/{routeid}/{userid}")
	public ResponseEntity<Boolean> likeRoute(@PathVariable int routeid, @PathVariable int userid, Requester requester) {

		if (requester instanceof LoggedUser) {

			User u = uServ.findById(userid);
			String username = ((LoggedUser) requester).getUsername();
			if (u.getUsername().equals(username)) {
				Route r = rService.findByRouteId(routeid);
				boolean liked = likeService.likeRoute(r, u);
				return ResponseEntity.ok().body(liked);
			} else {
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
			}

		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

	}

	@GetMapping("/isRouteLiked/{routeid}/{userid}")
	public ResponseEntity<Boolean> isRouteLiked(@PathVariable int routeid, @PathVariable int userid) {
		User u = uServ.findById(userid);
		Route r = rService.findByRouteId(routeid);

		boolean isLiked = likeService.isRouteLiked(r, u);

		return ResponseEntity.ok().body(isLiked);
	}

	@GetMapping("/getRoute/{id}")
	public ResponseEntity<Object> getRoute(@PathVariable int id) {
		return ResponseEntity.ok().body(rService.findByRouteId(id));

	}

	/**
	 * @GetMapping("/getRoute/{id}") public ResponseEntity<Object>
	 * getRoute(@PathVariable int id, Requester requester) {
	 * 
	 * if (requester instanceof LoggedUser) {
	 * 
	 * return ResponseEntity.ok().body(rService.findByRouteId(id)); } else { return
	 * ResponseEntity.status(HttpStatus.UNAUTHORIZED).build(); } }
	 **/

	@PostMapping("/addComment")
	public ResponseEntity<String> addCommentToRoute(@RequestParam int routeid, @RequestParam int userid,
			@RequestParam String description, Requester requester) {
		if (requester instanceof LoggedUser) {

			User u = uServ.findById(userid);
			String username = ((LoggedUser) requester).getUsername();
			if (u.getUsername().equals(username)) {
				boolean isInserted = commentService.saveComment(description, userid, routeid);
				if (isInserted) {
					return ResponseEntity.ok().build();
				}
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User or Route does not exist!");
			} else {
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
			}

		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

	}

	@GetMapping("/getRouteLikes/{id}")
	public @ResponseBody int getRouteLikes(@PathVariable int id) {
		return likeService.getRouteLikes(id);
	}

	@GetMapping("/getUserLikedRoutes/{userId}")
	public @ResponseBody List<Route> getUserLikedRoutes(@PathVariable int userId) {
		return likeService.getUserLikedRoutes(userId);
	}

	@GetMapping("/getUserCreatedRoutes/{userId}")
	public @ResponseBody List<Route> getUserCreatedRoutes(@PathVariable int userId) {
		return rService.getUserCreatedRoutes(userId);
	}

}

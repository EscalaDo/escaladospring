package escalado.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class MainController {

	
	@GetMapping(path="/test")
	public @ResponseBody String addGameContents() {
		return "Spring funciona perfectament!";
	}
	
}

package escalado.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import escalado.Models.DifficultyLevel;
import escalado.Services.DifficultyLevelService;

@Controller
public class DifficultyLevelController {
	
	@Autowired
	DifficultyLevelService diffLevelService;
	
	@GetMapping("/getGrades")
	public @ResponseBody List<DifficultyLevel> getAllExercises() {
		return diffLevelService.findAll();
	}
}

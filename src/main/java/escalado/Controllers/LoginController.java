package escalado.Controllers;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;

import escalado.Controllers.Requests.LoginHttpRequest;
import escalado.Exceptions.InvalidAccessTokenException;
import escalado.Exceptions.InvalidPasswordException;
import escalado.Exceptions.UserDoesNotExistException;
import escalado.Models.User;
import escalado.Services.UserService;
import escalado.Services.Auth.JwtTokenVerifierService;
import escalado.Utils.LoginResponse;

@Controller
public class LoginController {

	@Autowired
	UserService userService;
	
	@Autowired
	JwtTokenVerifierService tokenDeGenerator;
	
	@PostMapping("auth/login")
	public ResponseEntity<LoginResponse> login(@RequestBody LoginHttpRequest request){
		
		try {
			String token = userService.login(request.getUsername(), request.getPassword());
			Optional<User> u = userService.getUserByUsername(request.getUsername());
			return ResponseEntity.ok(new LoginResponse(token, u.get()));
		} catch (UserDoesNotExistException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new LoginResponse(e.getMessage())); 
		} catch(InvalidPasswordException e1) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new LoginResponse(e1.getMessage()));
		}
		
	}
	
	@GetMapping("/auth")
	public ResponseEntity<LoginResponse> auth(@RequestHeader("Authorization") String token ){
		try {
			String tokenNoBearer = token.replace("Bearer ", "");
			Optional<User> u = userService.getUserByUsername(tokenDeGenerator.verify(tokenNoBearer));
			return ResponseEntity.ok(new LoginResponse(token, u.get()));
		} catch(NoSuchElementException e3) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new LoginResponse(e3.getMessage()));
		} catch(SignatureVerificationException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new LoginResponse(e.getMessage()));
		} catch(TokenExpiredException e1) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new LoginResponse(e1.getMessage()));
		} catch (InvalidAccessTokenException e2) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(new LoginResponse(e2.getMessage()));
		} 
				
	}
	
}

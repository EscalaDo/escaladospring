package escalado.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import escalado.Models.Comment;
import escalado.Services.CommentService;

@Controller
public class CommentController {

	@Autowired
	CommentService comServ;
	
	@GetMapping("/commentsFromRoute/{id}")
	public @ResponseBody List<Comment> comments (@PathVariable Integer id){
		return comServ.GetCommentsFromRoute(id);
	}
}

package escalado.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import escalado.Models.Skill;
import escalado.Services.SkillService;

@Controller
public class SkillController {

	@Autowired
	SkillService skillService;
	
	@GetMapping("/getSkills")
	public @ResponseBody List<Skill> getSkills() {
		return skillService.findAll();
	}
}

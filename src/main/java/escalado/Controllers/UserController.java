package escalado.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import escalado.Models.Role;
import escalado.Services.RoleService;
import escalado.Services.UserService;

@Controller
public class UserController {
	@Autowired
	UserService uServ;
	
	@Autowired
	RoleService roleServ;
	
	@GetMapping("/getRoles")
	public @ResponseBody List<Role> addGameContents() {
		return roleServ.findAll();
	}
	
}

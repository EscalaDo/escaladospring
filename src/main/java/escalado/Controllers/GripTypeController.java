package escalado.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import escalado.Models.GripType;
import escalado.Services.GripTypeService;

@Controller
public class GripTypeController {

	@Autowired
	GripTypeService gripTypeService;
	
	@GetMapping("/getGripTypes")
	public @ResponseBody List<GripType> getGripTypes() {
		return gripTypeService.findAll();
	}
}

package escalado.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import escalado.Exceptions.UserDoesNotExistException;
import escalado.Models.Goal;
import escalado.Services.GoalService;

@Controller
public class GoalController {

	@Autowired
	GoalService goalService;
	
	@GetMapping("/goals")
	public @ResponseBody List<Goal> goals() {
		return goalService.findAll();
	}

	@PostMapping("/createGoal")
	public ResponseEntity<String> createGoal(@RequestParam("description") String description, @RequestParam("userId") Integer userId,  
			@RequestParam("achieved") boolean achieved){
		try {
			goalService.insertar(description, 1, achieved);
		} catch (UserDoesNotExistException e) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().build();
	}
}

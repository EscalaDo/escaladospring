package escalado.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import escalado.Models.Training;
import escalado.Models.User;
import escalado.Services.TrainingService;
import escalado.Services.UserService;
import escalado.Services.Auth.Requester.LoggedUser;
import escalado.Services.Auth.Requester.Requester;

@Controller
public class TrainingController {

	@Autowired
	TrainingService trainingService;

	@Autowired
	UserService userService;

	@GetMapping("/getAllTrainings")
	public @ResponseBody List<Training> getAllTrainings() {
		return trainingService.findAll();
	}

	@PostMapping("/createTraining")
	public ResponseEntity<String> createTraining(@RequestParam("userId") Integer userId,
			@RequestParam("name") String name, @RequestParam("nameEx") String[] nameEx,
			@RequestParam("sets") Integer[] sets, @RequestParam("reps") Integer[] reps, Requester requester) {

		if (requester instanceof LoggedUser) {
			User u = userService.findById(userId);
			String username = ((LoggedUser) requester).getUsername();
			if (u.getUsername().equals(username)) {
				trainingService.insert(name, u, nameEx, sets, reps);
				return ResponseEntity.ok().build();
			} else {
				return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
			}

		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}

	}

	@GetMapping("/getUserLikedTrainings/{userId}")
	public @ResponseBody List<Training> getUserLikedTrainings(@PathVariable int userId) {
		return trainingService.userLikedTrainings(userId);
	}

	@GetMapping("/getUserCreatedTrainings/{userId}")
	public @ResponseBody List<Training> getUserCreatedTrainings(@PathVariable int userId) {
		return trainingService.userCreatedTrainings(userId);
	}

	@PostMapping("/saveTrainingForUser/{trainingId}/{userId}")
	public @ResponseBody ResponseEntity<Boolean> saveTrainingForUser(@PathVariable int trainingId,
			@PathVariable int userId) {
		return ResponseEntity.ok().body(trainingService.saveTrainingForUser(trainingId, userId));
	}

	@GetMapping("/getTraining/{trainingId}")
	public @ResponseBody Training getTrainingById(@PathVariable int trainingId) {
		return trainingService.findById(trainingId);
	}
}

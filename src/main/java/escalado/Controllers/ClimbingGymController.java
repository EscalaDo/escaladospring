package escalado.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import escalado.Models.ClimbingGym;
import escalado.Services.ClimbingGymService;

@Controller
public class ClimbingGymController {

	@Autowired
	ClimbingGymService cgService;

	@GetMapping("/climbingGyms")
	public @ResponseBody List<ClimbingGym> getAllExercises() {
		return cgService.findAll();
	}

	@PostMapping("/climbingGym")
	public ResponseEntity<String> createClimbingGym(@RequestParam("name") String name,
			@RequestParam("location") String location) {

		cgService.insertar(name, location);
		return ResponseEntity.ok().build();
	}
}

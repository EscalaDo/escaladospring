package escalado.Exceptions;

public class UsernameAlreadyExistsException extends Exception {
	
	private static final long serialVersionUID = 1L;

    public UsernameAlreadyExistsException(String username) {
        super("The username " + username + " already exists");
    }
}

package escalado.Exceptions;

public class UserDoesNotExistException extends Exception {

	// la checked obliga a indicar un throw exception, la unchecked no, entonces
	// mejor que extienda de Runtime en este caso
	private static final long serialVersionUID = 1L;

	public UserDoesNotExistException() {
		super("The user does not exist.");
	}

	public UserDoesNotExistException(String username) {
		super("The user with username " + username + " does not exist.");
	}

	public UserDoesNotExistException(Throwable t) {
		super(t);
	}

	public UserDoesNotExistException(String message, Throwable t) {
		super(message, t);
	}
}
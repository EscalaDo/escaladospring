package escalado.Exceptions;

public class EntityDoesNotExistException extends Exception {

	private static final long serialVersionUID = 1L;

	public EntityDoesNotExistException(String entity) {
		super("The entity " + entity + " does not exist");
	}

}

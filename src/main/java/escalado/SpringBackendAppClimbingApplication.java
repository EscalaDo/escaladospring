package escalado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBackendAppClimbingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBackendAppClimbingApplication.class, args);
	}

}

package escalado.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import escalado.Exceptions.EmptyFieldException;
import escalado.Exceptions.EntityDoesNotExistException;
import escalado.Exceptions.InvalidPasswordException;
import escalado.Exceptions.UserDoesNotExistException;
import escalado.Exceptions.UsernameAlreadyExistsException;
import escalado.Models.ClimbingGym;
import escalado.Models.Role;
import escalado.Models.User;
import escalado.Repositories.ClimbingGymRepository;
import escalado.Repositories.RoleRepository;
import escalado.Repositories.UserRepository;
import escalado.Services.Auth.BcryptPasswordEncoderService;
import escalado.Services.Auth.JwtTokenGeneratorService;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	ClimbingGymRepository climbingGymRepo;

	@Autowired
	BcryptPasswordEncoderService encoder;

	@Autowired
	JwtTokenGeneratorService tokenGenerator;

	public User findById(Integer id) {
		return userRepository.findById(id).orElse(null);
	}

	public List<User> findAll() {
		return userRepository.findAll();
	}

	public void delete(Integer id) {
		userRepository.deleteById(id);
	}

	public User insert(User j) {
		return userRepository.save(j);
	}

	public User edit(User j) {
		return userRepository.save(j);
	}

	public void register(String username, String name, String lastname, String email, String password, int roleId,
			String location) throws UsernameAlreadyExistsException, EntityDoesNotExistException {
		if (userRepository.existsByUsername(username)) {
			throw new UsernameAlreadyExistsException(username);
		}

		Optional<Role> role = roleRepository.findById(roleId);
		if (role.isPresent()) {
			if (role.get().getName().equalsIgnoreCase("Climbing Gym")) {
				if (location == null) {
					throw new EmptyFieldException();
				}
				
				ClimbingGym gym = new ClimbingGym(username, location);
				climbingGymRepo.save(gym);
			}
			String encodedPass = encoder.encode(password);
			User newUser = new User(name, lastname, username, email, role.get(), encodedPass);
			insert(newUser);
		} else {
			throw new EntityDoesNotExistException("role with id " + roleId);
		}
	}

	public String login(String username, String password) throws UserDoesNotExistException, InvalidPasswordException {
		Optional<User> user = getUserByUsername(username);
		if (user.isPresent()) {
			User u = user.get();
			if (encoder.match(password, u.getPassword())) {
				return tokenGenerator.generate(username, u.getUserRole());
			} else {
				throw new InvalidPasswordException();
			}
		} else {
			throw new UserDoesNotExistException(username);
		}
	}

	public Optional<User> getUserByUsername(String username) {
		Optional<User> user = userRepository.findByUsername(username);
		return user;
	}

}

package escalado.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import escalado.Exceptions.UserDoesNotExistException;
import escalado.Models.Goal;
import escalado.Models.User;
import escalado.Repositories.GoalRepository;
import escalado.Repositories.UserRepository;

@Service
public class GoalService {

	@Autowired
	GoalRepository goalRepository;

	@Autowired
	UserRepository userRepo;

	public Goal findById(Integer id) {
		return goalRepository.findById(id).orElse(null);
	}

	public List<Goal> findAll() {
		return goalRepository.findAll();
	}

	public void delete(Integer id) {
		goalRepository.deleteById(id);
	}

	public Goal insertar(String description, int userId, boolean achieved) throws UserDoesNotExistException {
		Optional<User> goalPublisher = userRepo.findById(userId);
		if (goalPublisher.isPresent()) {
			Goal j = new Goal(description, goalPublisher.get(), achieved);
			return goalRepository.save(j);
		} else {
			throw new UserDoesNotExistException();
		}

	}

	public Goal editar(Goal j) {
		return goalRepository.save(j);
	}
}

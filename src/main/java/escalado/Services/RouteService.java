package escalado.Services;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import escalado.Models.ClimbingGym;
import escalado.Models.DifficultyLevel;
import escalado.Models.GripType;
import escalado.Models.Image;
import escalado.Models.Route;
import escalado.Models.Skill;
import escalado.Models.User;
import escalado.Repositories.RouteRepository;
import escalado.Utils.HttpResponseData;

@Service
public class RouteService {
	private final static String PATH = "/home/debian/EscaladoImages/";
	//private final static String PATH = "/Users/amina/imagesdebian";

	@Autowired
	ImageService iServ;

	@Autowired
	RouteRepository routeRepository;

	public List<Route> findAll() {
		List<Route> routes = routeRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));

		return routes;

	}

	public Route findByRouteId(Integer id) {
		Route route = routeRepository.findById(id).orElse(null);
		return route;
	}

	public Route addRoute(Route route) {
		return routeRepository.save(route);
	}

	public Route editRoute(Route route) {
		return routeRepository.save(route);
	}
	
	public boolean deleteRouteById(int id) {
		Optional<Route> route = routeRepository.findById(id);
		if(route.isPresent()) {
			routeRepository.deleteRouteById(id);
			return true;
		}
		return false;
	}
	
	public HttpResponseData addOneImage(MultipartFile file) {
		
		try {
			//String filepath = PATH + u.getId()+"/";
		
				String[] cType = file.getContentType().split("/");
				String fileName = String.valueOf(System.currentTimeMillis()) + "." + cType[1];
				String newFilePath = PATH + fileName;
				File newImageFile = new File(newFilePath);
				try {
					file.transferTo(newImageFile);
					Image i = new Image(fileName);
					i.setRoute(findByRouteId(1));
					iServ.insert(i);
				} catch (IllegalStateException | IOException e) {
					// chivato
				}
			

			return new HttpResponseData(HttpStatus.OK, "OK");
		} catch (Exception e) {
			return new HttpResponseData(HttpStatus.EXPECTATION_FAILED, "Could not upload the file!");
		}
		
	}

	// afegir una ruta
	public HttpResponseData userAddRoute(MultipartFile[] files, User u, Skill sk, GripType gt, DifficultyLevel dl,
			String description, Integer attempts, ClimbingGym climbingGym) {
		String message = "";
		Route route = new Route(attempts, description, u, sk, gt, dl, climbingGym);
		addRoute(route);

		if (u == null) {
			message = "User does not exist...";
		}

		try {
			//String filepath = PATH + u.getId()+"/";
			Arrays.asList(files).stream().forEach((file) -> {
				String[] cType = file.getContentType().split("/");
				String fileName = String.valueOf(System.currentTimeMillis()) + "." + cType[1];
				String newFilePath = PATH + fileName;
				File newImageFile = new File(newFilePath);
				try {
					file.transferTo(newImageFile);
					Image i = new Image(fileName);
					i.setRoute(route);
					iServ.insert(i);
				} catch (IllegalStateException | IOException e) {
					// chivato
				}
			});

			return new HttpResponseData(HttpStatus.OK, message);
		} catch (Exception e) {
			message = "Could not upload the file!";
			return new HttpResponseData(HttpStatus.EXPECTATION_FAILED, message);
		}
	}

	// retorna totes les vies per a la feed
	public List<Route> getAllRoutes() {
		return findAll();
	}
	
	public List<Route> getUserCreatedRoutes(int id){
		return routeRepository.getRoutesUserCreated(id);
	}

}

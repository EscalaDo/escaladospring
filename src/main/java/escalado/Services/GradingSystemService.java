package escalado.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import escalado.Models.GradingSystem;
import escalado.Repositories.GradingSystemRepository;

@Service
public class GradingSystemService {

	@Autowired
	GradingSystemRepository gsRepo;
	
	public GradingSystem findById(Integer id) {
		return gsRepo.findById(id).orElse(null);
	}

	public List<GradingSystem> findAll() {
		return gsRepo.findAll();
	}
	
	public void delete(Integer id) {
		gsRepo.deleteById(id); 
	}
	
	public GradingSystem insert(GradingSystem j) {
		return gsRepo.save(j);
	}

	public GradingSystem edit(GradingSystem j) {
		return gsRepo.save(j);
	}
	
}

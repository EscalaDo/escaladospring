package escalado.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import escalado.Models.DifficultyLevel;
import escalado.Repositories.DifficultyLevelRepository;

@Service
public class DifficultyLevelService {

	@Autowired
	DifficultyLevelRepository dlRepository;
	
	public DifficultyLevel findById(Integer id) {
		return dlRepository.findById(id).orElse(null);
	}

	public List<DifficultyLevel> findAll() {
		return dlRepository.findAll();
	}
	
	public void delete(Integer id) {
		dlRepository.deleteById(id); 
	}
	
	public DifficultyLevel insert(DifficultyLevel j) {
		return dlRepository.save(j);
	}

	public DifficultyLevel edit(DifficultyLevel j) {
		return dlRepository.save(j);
	}
	
}

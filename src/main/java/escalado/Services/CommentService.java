package escalado.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import escalado.Models.Comment;
import escalado.Models.Route;
import escalado.Models.User;
import escalado.Repositories.CommentRepository;

@Service
public class CommentService {

	@Autowired
	CommentRepository commentRepository;
	
	@Autowired
	UserService uServ;
	
	@Autowired
	RouteService rServ;
	
	public Comment findById(Integer id) {
		return commentRepository.findById(id).orElse(null);
	}

	public List<Comment> findAll() {
		return commentRepository.findAll();
	}
	
	public void delete(Integer id) {
		commentRepository.deleteById(id); 
	}
	
	public Comment insertar(Comment j) {
		return commentRepository.save(j);
	}

	public Comment editar(Comment j) {
		return commentRepository.save(j);
	}

	public List<Comment> GetCommentsFromRoute(Integer idRoute){	
		return commentRepository.getCommentsFromRoute(idRoute);
	}
	
	public boolean saveComment(String description, int user, int route) {
		User u = uServ.findById(user);
		Route r = rServ.findByRouteId(route);
		if(u==null) {
			return false;
		} else if(r==null){
			return false;
		}
		Comment c = new Comment(description, u, r);
		commentRepository.save(c);
		return true;
	}
}

package escalado.Services.Auth;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import escalado.Models.Role;

@Service
public class JwtTokenGeneratorService {
	
	@Value("escalado.app.jwtSecret")
	private String signingSecretKey;
	
	public String generate(String username, Role role) {
		return JWT.create()
				.withIssuer(username)
				.withExpiresAt(Date.from(Instant.now().plus(7, ChronoUnit.DAYS)))
				.withClaim("role", role.getName())
				.sign(Algorithm.HMAC256(signingSecretKey))
				.toString();
	}

}

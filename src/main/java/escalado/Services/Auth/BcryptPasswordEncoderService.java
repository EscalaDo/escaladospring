package escalado.Services.Auth;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
public class BcryptPasswordEncoderService {

	private static final int SALT_ITERATIONS = 11;

	public String encode(String plainPassword) {
		return BCrypt.hashpw(plainPassword, BCrypt.gensalt(SALT_ITERATIONS));
	}

	public boolean match(String plainPassword, String encodedPassword) {
		return BCrypt.checkpw(plainPassword, encodedPassword);
	}
}

package escalado.Services.Auth.Requester;

public class LoggedUser extends Requester {

	// esto lo ponemos para controlar que el usuario loggeado solo pueda obtener
	// información relacionada con él
	private String username;

	public LoggedUser(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}
	
	
}

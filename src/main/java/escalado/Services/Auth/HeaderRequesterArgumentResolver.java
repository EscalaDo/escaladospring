package escalado.Services.Auth;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import escalado.Exceptions.ExpiredAccessTokenException;
import escalado.Exceptions.InvalidAccessTokenException;
import escalado.Services.Auth.Requester.AnonymousUser;
import escalado.Services.Auth.Requester.LoggedUser;
import escalado.Services.Auth.Requester.Requester;

public class HeaderRequesterArgumentResolver implements HandlerMethodArgumentResolver {

	private JwtTokenVerifierService tokenVerifier;

	public HeaderRequesterArgumentResolver(JwtTokenVerifierService tokenVerifier) {
		this.tokenVerifier = tokenVerifier;
	}

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.getParameterType() == Requester.class;
	}

	/**
	 * Este método obtiene el header de authorization y comprueba que el token sea
	 * valido, si es válido devolvemos su user, sino anounymous
	 */
	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
		try {
			String header = webRequest.getHeader("Authorization");
			if (header != null) {
				String token = header.replace("Bearer ", "");
				String username = tokenVerifier.verify(token);
				return new LoggedUser(username);
			} else {
				return new AnonymousUser();
			}
		} catch (InvalidAccessTokenException | ExpiredAccessTokenException e) {
			return new AnonymousUser();
		}
	}

}

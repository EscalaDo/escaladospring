package escalado.Services.Auth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ArgumentResolverConfiguration implements WebMvcConfigurer {
	@Autowired
	JwtTokenVerifierService tokenVerifier;

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {

		resolvers.add(new HeaderRequesterArgumentResolver(tokenVerifier));
	}
}

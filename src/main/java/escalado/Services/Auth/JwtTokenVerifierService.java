package escalado.Services.Auth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;

import escalado.Exceptions.ExpiredAccessTokenException;
import escalado.Exceptions.InvalidAccessTokenException;

@Service
public class JwtTokenVerifierService {

	@Value("escalado.app.jwtSecret")
	private String signingSecretKey;
	
	public String verify(String token) {
		try {
		DecodedJWT t =	JWT.require(Algorithm.HMAC256(signingSecretKey)).build().verify(token);
			return t.getIssuer();
		} catch(SignatureVerificationException e) {
			throw new InvalidAccessTokenException();
		} catch (TokenExpiredException e1) {
			throw new ExpiredAccessTokenException();
		} catch (InvalidAccessTokenException e2) {
			throw new InvalidAccessTokenException();
		}
		
	}
}

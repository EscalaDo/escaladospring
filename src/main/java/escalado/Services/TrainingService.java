package escalado.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import escalado.Models.Exercise;
import escalado.Models.Training;
import escalado.Models.User;
import escalado.Repositories.ExerciseRepository;
import escalado.Repositories.TrainingRepository;

@Service
public class TrainingService {

	@Autowired
	TrainingRepository trainingRepository;

	@Autowired
	ExerciseRepository exRepository;
	
	@Autowired
	UserService userServ;
	
	public Training findById(Integer id) {
		return trainingRepository.findById(id).orElse(null);
	}

	public List<Training> findAll() {
		return trainingRepository.allTrainingsDescOrder();
	}

	public void delete(Integer id) {
		trainingRepository.deleteById(id);
	}

	public Training insert(String name, User u, String[] nameEx, Integer[] sets, Integer[] reps) {
		Training t = new Training(name, u);
		trainingRepository.save(t);
		for (int i=0;i<nameEx.length;i++) {
			Exercise ex = new Exercise(nameEx[i], sets[i], reps[i], t);
			exRepository.save(ex);
		}
		return trainingRepository.save(t);
	}

	public Training edit(Training j) {
		return trainingRepository.save(j);
	}
	
	public Training insertTraining(Training t) {
		return trainingRepository.save(t);
	}
	
	public List<Training> userLikedTrainings(int id){
		return trainingRepository.getTrainingsUserLiked(id);
	}

	public List<Training> userCreatedTrainings(int id){
		return trainingRepository.getTrainingsUserCreated(id);
	}
	//return true if created, false if deleted
	public Boolean saveTrainingForUser(int trainingId, int userId) {
		if(trainingRepository.isTrainingUserSaved(trainingId, userId) == 0) {
			User u = userServ.findById(userId);
			Training t = findById(trainingId);
			u.getTrainings().add(t);
			userServ.edit(u);
			return true;
		} else {
			User u = userServ.findById(userId);
			Training t = findById(trainingId);
			u.getTrainings().remove(t);
			userServ.edit(u);
			return false;
		}
		
	}
}

package escalado.Services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import escalado.Models.Like;
import escalado.Models.Route;
import escalado.Models.User;
import escalado.Repositories.LikeRepository;

@Service
public class LikeService {

	@Autowired
	LikeRepository likeRepository;

	public Like findById(Integer id) {
		return likeRepository.findById(id).orElse(null);
	}

	public boolean likeRoute(Route route, User user) {

		int l = likeRepository.doesLikeExist(route.getId(), user.getId());
		if (l == 1) {
			likeRepository.deleteLikeRoute(route.getId(), user.getId());
			return false;
		} else {
			likeRepository.likeRoute(route.getId(), user.getId());
			return true;
		}
	}

	public int getRouteLikes(int id) {
		return likeRepository.getLikesOfPost(id);
	}

	public boolean isRouteLiked(Route route, User user) {
		int l = likeRepository.doesLikeExist(route.getId(), user.getId());
		if (l == 1) {
			return true;
		}
		return false;
	}

	public List<Route> getUserLikedRoutes(int userId) {

		List<Like> likes = likeRepository.findAll();
		List<Route> likesOfUser = new ArrayList<Route>();
		for (Like like : likes) {
			if (like.getUser().getId() == userId) {
				likesOfUser.add(like.getRoute());
			}
		}
		return likesOfUser;
	}
}

package escalado.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import escalado.Models.Exercise;
import escalado.Models.Training;
import escalado.Repositories.ExerciseRepository;

@Service
public class ExerciseService {

	@Autowired
	ExerciseRepository exerciseRepository;

	public Exercise findById(Integer id) {
		return exerciseRepository.findById(id).orElse(null);
	}

	public List<Exercise> findAll() {
		return exerciseRepository.findAll();
	}

	public void delete(Integer id) {
		exerciseRepository.deleteById(id);
	}

	public Exercise insertar(String name, int sets, int reps, Training training) {
		Exercise e = new Exercise(name, sets, reps, training);
		return exerciseRepository.save(e);
	}

	public Exercise editar(Exercise e) {
		return exerciseRepository.save(e);
	}

	public Exercise insertExercise(Exercise e) {
		return exerciseRepository.save(e);
	}

	public Boolean deleteExerciseFromTraining(int exerciseId) {

		Optional<Exercise> ex = exerciseRepository.findById(exerciseId);
		if (ex.isPresent()) {
			Exercise e = ex.get();
			boolean p = e.getTraining().getExercises().remove(e);
			if (p) {
				exerciseRepository.deleteById(exerciseId);
				return true;
			}
			return false;
		}
		return false;
	}
}

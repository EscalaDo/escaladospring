package escalado.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import escalado.Models.Skill;
import escalado.Repositories.SkillRepository;

@Service
public class SkillService {

	@Autowired
	SkillRepository skillRepository;
	
	public Skill findById(Integer id) {
		return skillRepository.findById(id).orElse(null);
	}

	public List<Skill> findAll() {
		return skillRepository.findAll();
	}
	
	public void delete(Integer id) {
		skillRepository.deleteById(id); 
	}
	
	public Skill insert(Skill j) {
		return skillRepository.save(j);
	}

	public Skill edit(Skill j) {
		return skillRepository.save(j);
	}
	
}

package escalado.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import escalado.Models.GripType;
import escalado.Repositories.GripTypeRepository;


@Service
public class GripTypeService {

	@Autowired
	GripTypeRepository gripTypeRepository;
	
	public GripType findById(Integer id) {
		return gripTypeRepository.findById(id).orElse(null);
	}

	public List<GripType> findAll() {
		return gripTypeRepository.findAll();
	}
	
	public void delete(Integer id) {
		gripTypeRepository.deleteById(id); 
	}
	
	public GripType insert(GripType j) {
		return gripTypeRepository.save(j);
	}

	public GripType edit(GripType j) {
		return gripTypeRepository.save(j);
	}
	
}

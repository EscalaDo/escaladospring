package escalado.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import escalado.Models.Role;
import escalado.Repositories.RoleRepository;

@Service
public class RoleService {

	@Autowired
	RoleRepository rRepository;
	
	public Role findById(Integer id) {
		return rRepository.findById(id).orElse(null);
	}

	public List<Role> findAll() {
		return rRepository.findAll();
	}
	
	public void delete(Integer id) {
		rRepository.deleteById(id); 
	}
	
	public Role insert(Role j) {
		return rRepository.save(j);
	}

	public Role edit(Role j) {
		return rRepository.save(j);
	}
	
}

package escalado.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import escalado.Models.ClimbingGym;
import escalado.Repositories.ClimbingGymRepository;

@Service
public class ClimbingGymService {

	@Autowired
	ClimbingGymRepository cgRepository;
	
	public ClimbingGym findById(Integer id) {
		return cgRepository.findById(id).orElse(null);
	}

	public List<ClimbingGym> findAll() {
		return cgRepository.findAll();
	}
	
	public void delete(Integer id) {
		cgRepository.deleteById(id); 
	}
	
	public ClimbingGym insertar(String name, String location) {
		ClimbingGym c = new ClimbingGym(name, location);
		return cgRepository.save(c);
	}

	public ClimbingGym insertAsObj(ClimbingGym j) {
		return cgRepository.save(j);
	}
	
}

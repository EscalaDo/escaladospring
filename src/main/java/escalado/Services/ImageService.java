package escalado.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import escalado.Models.Image;
import escalado.Repositories.ImageRepository;

@Service
public class ImageService {

	@Autowired
	ImageRepository imageRepository;
	
	public Image findById(Integer id) {
		return imageRepository.findById(id).orElse(null);
	}

	public List<Image> findAll() {
		return imageRepository.findAll();
	}
	
	public void delete(Integer id) {
		imageRepository.deleteById(id); 
	}
	
	public Image insert(Image j) {
		return imageRepository.save(j);
	}

	public Image edit(Image j) {
		return imageRepository.save(j);
	}
	
}

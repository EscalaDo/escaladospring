package escalado;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import escalado.Models.ClimbingGym;
import escalado.Models.DifficultyLevel;
import escalado.Models.GripType;
import escalado.Models.Route;
import escalado.Models.Skill;
import escalado.Models.User;
import escalado.Repositories.RouteRepository;
import escalado.Services.ImageService;
import escalado.Services.RouteService;
import escalado.Utils.HttpResponseData;

@ExtendWith(MockitoExtension.class)
public class RouteServiceTest {

	private static final int ROUTE_ID = 1;

	@Mock
	private ImageService iServ;

	@Mock
	private RouteRepository routeRepository;

	@InjectMocks
	private RouteService rServ;

	@Test
	void returnsRouteById() {
		Route route = new Route();
		route.setId(ROUTE_ID);
		when(routeRepository.findById(ROUTE_ID)).thenReturn(Optional.of(route));
		
		Route r = rServ.findByRouteId(ROUTE_ID);
		
		assertEquals(r.getId(), ROUTE_ID);
	}
	
	@Test
	void shouldAddRoute() {
		MultipartFile[] files = new MultipartFile[1];
		files[0] = new MockMultipartFile("test", null, "test/test", (byte[]) null);
		User u = new User();
		Skill sk = new Skill();
		GripType gt = new GripType();
		DifficultyLevel dl = new DifficultyLevel();
		String description = "test route";
		Integer attempts = 1;
		ClimbingGym climbingGym = new ClimbingGym();
		
		//cuando llame al routeRepository que no haga nada
		when(routeRepository.save(any())).thenReturn(null);
		
		HttpResponseData d = rServ.userAddRoute(files, u, sk, gt, dl, description, attempts, climbingGym);
		
		//comprobamos que se llama a los servicios que debería
		verify(routeRepository).save(any());
		
		assertEquals(d.getStatus(), HttpStatus.OK);
		assertEquals(d.getMessage(), "");
	}

}
